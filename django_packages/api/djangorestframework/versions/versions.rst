.. index::
   pair: DRF ; versions


.. _drf_versions:

============================================
Django Rest Framework (DRF) versions
============================================

.. seealso::

   - https://github.com/encode/django-rest-framework/releases

.. toctree::
   :maxdepth: 3

   3.9.3/3.9.3
   3.9.0/3.9.0
