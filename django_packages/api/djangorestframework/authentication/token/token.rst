
.. index::
   pair: token; authentication
   pair: jeton; authentication
   pair: jeton ; drf_create_token
   ! DRF Token
   ! Token


.. _drf_token_authentication:

========================================================================
DRF token authentication
========================================================================

.. seealso::

   - https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication
   - https://www.django-rest-framework.org/api-guide/authentication/
   - https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html





Introduction
===============

This authentication scheme uses a simple token-based HTTP Authentication
scheme.

Token authentication is appropriate for client-server setups, such as
native desktop and mobile clients.

To use the TokenAuthentication scheme you'll need to configure the
authentication classes to include TokenAuthentication, and additionally
include rest_framework.authtoken in your INSTALLED_APPS setting::

    INSTALLED_APPS = [
        ...
        'rest_framework.authtoken'
    ]


The new **drf_create_token django** command for the authtoken application : Create DRF Token for a given user
===============================================================================================================

::

    python manage.py --help

::
    ...

    [auth]
        changepassword
        createsuperuser

    [authtoken]
        drf_create_token

    ...

    [rest_framework]
        generateschema

::

    python manage.py help drf_create_token

::

    usage: manage.py drf_create_token [-h] [-r] [--version] [-v {0,1,2,3}]
                                          [--settings SETTINGS]
                                          [--pythonpath PYTHONPATH] [--traceback]
                                          [--no-color] [--force-color]
                                          username

    Create DRF Token for a given user

    positional arguments:
      username

    optional arguments:
      -h, --help            show this help message and exit
      -r, --reset           Reset existing User token and create a new one
      --version             show program's version number and exit
      -v {0,1,2,3}, --verbosity {0,1,2,3}
                            Verbosity level; 0=minimal output, 1=normal output,
                            2=verbose output, 3=very verbose output
      --settings SETTINGS   The Python path to a settings module, e.g.
                            "myproject.settings.main". If this isn't provided, the
                            DJANGO_SETTINGS_MODULE environment variable will be
                            used.
      --pythonpath PYTHONPATH
                            A directory to add to the Python path, e.g.
                            "/home/djangoprojects/myproject".
      --traceback           Raise on CommandError exceptions
      --no-color            Don't colorize the command output.
      --force-color         Force colorization of the command output.


Create a user
===============

Go to the admin interface and create a user.


.. figure:: admin_tokens.png
   :align: center


To create a token for a user
================================

We have 2 choices:

1) create a token with the **drf_create_token** command
-----------------------------------------------------------

    python manage.py drf_create_token <username>

Exemple::


    pipenv run python manage.py drf_create_token test1

::

    Generated token 3cf04875a887c44fb526f5a5d3226571915e054b for user test1


2) create a token with the admin interface
----------------------------------------------


.. figure:: create_a_token.png
   :align: center


To renew a compromise token (**drf_create_token -r**)
========================================================

To renew a compromise token for <username>::

    python manage.py drf_create_token -r <username>


Example::

    python manage.py drf_create_token -r test1

::

    Generated token 04809b78c548af66759e2d2145f2e222fc3a56b2 for user test1



For clients to authenticate, the token key should be included in the
Authorization HTTP header.

The key should be prefixed by the string literal "Token", with whitespace
separating the two strings.

For example::

    Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b

Examples
===========

.. toctree::
   :maxdepth: 3

   examples/examples
