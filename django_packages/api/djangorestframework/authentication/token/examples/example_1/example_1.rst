
.. index::
   pair: DRF Token ; Example 1

.. _drf_token_example_1:

========================================================================
DRF token authentication example 1
========================================================================


::

    http -b "<hostname>/<app>/<function>/?xxx=aaa&version__gt=2.4.0" 'Authorization: Token af0866cc4b01833e96dfe84dffca945702b1c4a5'


The token is given with the following part::

    'Authorization: Token id0866cc4b01833e96dfe84dffca945702b1c4a5'
