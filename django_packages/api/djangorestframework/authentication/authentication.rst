
.. index::
   pair: Django Rest Framework; authentication


.. _drf_authentication:

========================================================================
DRF authentication
========================================================================


.. seealso::

   - https://www.django-rest-framework.org/api-guide/authentication
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/WWW-Authenticate





Definition
============


Authentication is the mechanism of associating an incoming request with
a set of identifying credentials, such as the user the request came from,
or the token that it was signed with.

The permission and throttling policies can then use those credentials
to determine if the request should be permitted.

Token
======

.. toctree::
   :maxdepth: 3

   token/token
