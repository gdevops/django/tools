
.. index::
   pair: token; simpleisbetter


.. _simpleisbetter_how_to_token:

========================================================================
how to implement token authentication using django-rest-framework
========================================================================

.. seealso::

   - https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html
   - https://github.com/sibtc/drf-token-auth-example
   - :ref:`simpleisbetter_how_to_jwt`




httpie use
===========

::

    http http://127.0.0.1:8000/hello/ 'Authorization: Token 9054f7aa9305e012b3c2300408c3dfdf390fcddf'


curl use
==========

The formatting looks weird and usually it is a point of confusion on
how to set this header. It will depend on the client and how to set the
HTTP request header.

For example, if we were using cURL, the command would be something like this::

    curl http://127.0.0.1:8000/hello/ -H 'Authorization: Token 9054f7aa9305e012b3c2300408c3dfdf390fcddf'


Python requests
=================

Or if it was a Python requests call:

::

    import requests

    url = 'http://127.0.0.1:8000/hello/'
    headers = {'Authorization': 'Token 9054f7aa9305e012b3c2300408c3dfdf390fcddf'}
    r = requests.get(url, headers=headers)



Angular
=========

::

    import { Injectable } from '@angular/core';
    import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
    import { Observable } from 'rxjs';

    @Injectable()
    export class AuthInterceptor implements HttpInterceptor {
      intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const user = JSON.parse(localStorage.getItem('user'));
        if (user && user.token) {
          request = request.clone({
            setHeaders: {
              Authorization: `Token ${user.accessToken}`
            }
          });
        }
        return next.handle(request);
      }
    }
