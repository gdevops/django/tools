
.. index::
   pair: DRF; Tutorials


.. _drf_tutorials:

========================================================================
Tutorials
========================================================================

- https://learndjango.com/tutorials/official-django-rest-framework-tutorial-beginners

.. toctree::
   :maxdepth: 3


   jwt_authentication/jwt_authentication
   official-django-rest-framework-tutorial-beginners/official-django-rest-framework-tutorial-beginners
   token_authentication/token_authentication
