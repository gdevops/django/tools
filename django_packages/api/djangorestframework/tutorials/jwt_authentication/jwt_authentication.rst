
.. index::
   pair: JWT; simpleisbetter


.. _simpleisbetter_how_to_jwt:

========================================================================
How to Use JWT Authentication with Django REST Framework
========================================================================

.. seealso::

   - https://simpleisbetterthancomplex.com/tutorial/2018/12/19/how-to-use-jwt-authentication-with-django-rest-framework.html
   - :ref:`simpleisbetter_how_to_token`





Introduction
==============

JWT stand for JSON Web Token and it is an authentication strategy used
by client/server applications where the client is a Web application
using JavaScript and some frontend framework like Angular, React or VueJS.
