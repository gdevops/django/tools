
.. index::
   ! DRF
   ! Django Rest Framework
   pair: Web ; API


.. _drf:

========================================================================
Django Rest Framework (DRF) Web APIs for Django
========================================================================

.. seealso::

   - https://github.com/encode/django-rest-framework
   - https://www.django-rest-framework.org
   - https://x.com/_tomchristie




Overview
==========

Django REST framework is a powerful and flexible toolkit for **building Web APIs**.

Some reasons you might want to use REST framework:

- The Web browsable API is a huge usability win for your developers.
- Authentication policies including optional packages for OAuth1 and OAuth2.
- Serialization that supports both ORM and non-ORM data sources.
- Customizable all the way down - just use regular function-based views if you
  don't need the more powerful features.
- Extensive documentation, and great community support.

There is a live example API for testing purposes, `available here`_.

.. _`available here`:  https://restframework.herokuapp.com/

Overview by Will Vincent
==========================

- see :ref:`drf_in_rest_api_django_2018`


Django Rest Framework
=====================

.. toctree::
   :maxdepth: 3


   authentication/authentication
   tutorials/tutorials
   versions/versions
