
.. _django_packages_API_intro:

============================================
Django packages API introduction
============================================

.. seealso::

   - https://restapiswithdjango.com/introduction/
   - :ref:`rest_api_django`


**The internet is powered by RESTful APIs**.

Behind the scenes even the simplest online task involves multiple computers
interacting with one another.

An **API (Application Programming Interface)** is a formal way to
describe two computers communicating directly with one another.

And while there are multiple ways to build an API, web APIs–which allow
for the transfer of data over the world wide web–are overwhelmingly
structured in a RESTful (REpresentational State Transfer) pattern.
