.. index::
   pair: Django ; API

.. _django_packages_API:

============================================
**Django packages API**
============================================

- https://restapiswithdjango.com/introduction/
- :ref:`rest_api_django`

.. toctree::
   :maxdepth: 4

   introduction/introduction
   django-hatchway/django-hatchway
   djangorestframework/djangorestframework
   fastapi/fastapi
