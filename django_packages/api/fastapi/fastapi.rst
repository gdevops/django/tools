.. index::
   pair: Django ; FastAPI

.. _django_fast_API:

============================================
Django and FastAPI
============================================

.. seealso::

   - :ref:`framework_fastapi`
   - https://github.com/erm/django-fastapi-example
   - :ref:`django_fastapi_2020_05_02`



Announce
==========

.. seealso::

   - :ref:`django_fastapi_2020_05_02`
