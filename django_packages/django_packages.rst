
.. index::
   pair: Django ; Packages


.. _django_packages:

============================================
Django packages
============================================

.. seealso::

   - https://www.djangopackages.com/

.. toctree::
   :maxdepth: 4

   api/api
   authentication/authentication
   forms/forms
