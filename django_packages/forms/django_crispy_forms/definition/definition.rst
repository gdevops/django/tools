


============================================
Django crispy forms definition
============================================

.. seealso::

   - https://django-crispy-forms.readthedocs.io/en/latest/
   - https://github.com/django-crispy-forms/django-crispy-forms/blob/master/README.rst


.. include:: README.rst
