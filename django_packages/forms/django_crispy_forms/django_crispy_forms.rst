
.. index::
   pair: Django ; Crispy Forms


.. _django_crispy_forms:

============================================
Django crispy forms
============================================

.. seealso::

   - https://github.com/django-crispy-forms/django-crispy-forms
   - https://djangopackages.org/packages/p/django-crispy-forms/
   - https://dotmobo.github.io/django-crispy-forms.html


.. toctree::
   :maxdepth: 3


   definition/definition
   versions/versions
