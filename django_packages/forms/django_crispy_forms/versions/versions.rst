
.. index::
   pair: Versions ; Django Crispy Forms


.. _django_crispy_forms_versions:

============================================
Django crispy forms versions
============================================

.. toctree::
   :maxdepth: 3


   1.8.0/1.8.0
