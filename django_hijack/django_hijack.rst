.. index::
   pair: HTML ; djhtml
   ! djhtml

.. _django_hijack:

=========================================================================================================================================
**django-hijack** (With Django Hijack, admins can log in and work on behalf of other users without having to know their credentials)
=========================================================================================================================================

- https://github.com/django-hijack/django-hijack
- https://www.mattlayman.com/blog/2020/hijack-to-help-customers/
