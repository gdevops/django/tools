.. index::
   pair: Versions ; sqlparse

.. _sqlparse_versions:

==============================================================
**sqlparse** versions
==============================================================

.. seealso::

   - https://github.com/andialbrecht/sqlparse/releases


.. toctree::
   :maxdepth: 3

   0.3.1/0.3.1
