.. index::
   pair: SQL ; sqlparse
   ! sqlparse

.. _sqlparse:

==============================================================
**sqlparse** (A non-validating SQL parser module for Python )
==============================================================

.. seealso::

   - https://github.com/andialbrecht/sqlparse
   - :ref:`django_sql_tips_2`


.. toctree::
   :maxdepth: 3

   examples/examples
   versions/versions
