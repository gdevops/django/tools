
.. _sqlparse_examples:

==============================================================
**sqlparse** examples
==============================================================




Example 1
==========

.. warning:: BE CAREFULL do not use {sql_query=} but {sql_query}


::

    # https://github.com/andialbrecht/sqlparse
    import sqlparse

    ...

    liste_fiches_weekly = FicheTemps.objects.filter(
        Q(created__gte=select_datetime_begin) & Q(created__lte=select_datetime_end),
        employe__login=employe.login,
    ).order_by("created")
    liste_fiches_weekly = liste_fiches_weekly.exclude(projet_id__in=projets_conges_id)

    sql_query = sqlparse.format(str(liste_fiches_weekly.query), reindent=True)
    logger.info(f"\nSQL query:\n{sql_query}\n")


::

    SELECT "fiche_temps"."id",
           "fiche_temps"."id_employe",
           "fiche_temps"."id_projet",
           "fiche_temps"."temps_impute",
           "fiche_temps"."commentaire",
           "fiche_temps"."etat",
           "fiche_temps"."created",
           "fiche_temps"."modified"
    FROM "fiche_temps"
    INNER JOIN "employe" ON ("fiche_temps"."id_employe" = "employe"."id")
    WHERE ("fiche_temps"."created" >= 2020-05-04 00:00:00+02:00
           AND "fiche_temps"."created" <= 2020-05-11 23:59:59+02:00
           AND "employe"."login" = pvergain
           AND NOT ("fiche_temps"."id_projet" IN (6,
                                                  9,
                                                  14,
                                                  13,
                                                  12,
                                                  10,
                                                  15,
                                                  8,
                                                  5,
                                                  4,
                                                  3,
                                                  2,
                                                  1,
                                                  11,
                                                  7)))
    ORDER BY "fiche_temps"."created" ASC


Example 2
===========

::

    liste_fiches_day = FicheTemps.objects.filter(
        employe__login=employe.login,
        created__year=created_fiche_temps.year,
        created__month=created_fiche_temps.month,
        created__day=created_fiche_temps.day,
    ).order_by("created")

    sql_query = sqlparse.format(str(liste_fiches_day.query), reindent=True)
    logger.info(f"\nSQL query:\n{sql_query}\n")


::

     SELECT "fiche_temps"."id",
            "fiche_temps"."id_employe",
            "fiche_temps"."id_projet",
            "fiche_temps"."temps_impute",
            "fiche_temps"."commentaire",
            "fiche_temps"."etat",
            "fiche_temps"."created",
            "fiche_temps"."modified"
     FROM "fiche_temps"
     INNER JOIN "employe" ON ("fiche_temps"."id_employe" = "employe"."id")
     WHERE (EXTRACT('day'
                    FROM"fiche_temps"."created" AT TIME ZONE 'Europe/Paris') = 6
            AND EXTRACT('month'
                        FROM"fiche_temps"."created" AT TIME ZONE 'Europe/Paris') = 5
            AND "fiche_temps"."created" BETWEEN 2020-01-01 00:00:00+01:00 AND 2020-12-31 23:59:59.999999+01:00
            AND "employe"."login" = pvergain)
     ORDER BY "fiche_temps"."created" ASC
