.. index::
   pair: HTML ; djLint
   ! djLint

.. _djlint:

===================================================================================================================================
**djLint** (Lint & Format HTML Templates, Django • Jinja • Nunjucks • Twig • Handlebars • Mustache • GoLang • Angular)
===================================================================================================================================

- https://github.com/Riverside-Healthcare/djLint
- https://github.com/Riverside-Healthcare/djLint/releases
- https://djlint.com/
- https://djlint.com/docs/configuration/
- https://djlint.com/docs/formatter/



Description
==============

Once upon a time all the other programming languages had a formatter and
linter.

Css, javascript, python, the c suite, typescript, ruby, php, go, swift,
and you know the others.

The cool kids on the block.

HTML templates were left out there on their own, in the cold, unformatted
and unlinted :( The dirty corner in your repository. Something had to change.

Grab it with pip::

    pip install djlint

Lint your project
=====================

::

    djlint . --extension=html.j2 --lint

Check your format
=====================

::

    djlint . --extension=html.j2 --check

Fix my format!
===================

::

    djlint . --extension=html.j2 --reformat


pre-commit
============


::

  - repo: https://github.com/Riverside-Healthcare/djLint
    rev: v1.31.0
    hooks:
      - id: djlint-reformat-django
      - id: djlint-django


Demo
======

- https://djlint.com/demo/
