
.. _django_ajax_brennan:

==========================================================================
**Fetching Data with AJAX and Django** by Brennan Tymrak
==========================================================================

.. seealso::

   - https://www.brennantymrak.com/articles/fetching-data-with-ajax-and-django.html
