
.. index::
   pair: Ajax ; Codifarticle

.. _codifarticle_ajax_fetch_example:

============================================
**Codifarticle AJAX** with fetch
============================================




frontend.js.utils.js
========================

.. literalinclude:: util.js
   :linenos:
   :language: javascript


articles.templates.articles.demande.achat_service.articles_update.html
==========================================================================

.. literalinclude:: articles_update.html
   :linenos:
   :language: django
