// utils.js
// Fonctions utilitaires Javascript
//
// - https://flaviocopes.com/javascript-let-const/
// - https://gdevops.frama.io/web/javascript/reference/instructions/const/const.html
// - https://gdevops.frama.io/web/javascript/reference/promise/flaviocopes.com/flaviocopes.com.html
// - https://gdevops.frama.io/web/linkertree/html/apis/document/methods/getElementById/getElementById.html
// - https://gdevops.frama.io/web/linkertree/html/apis/fetch/fetch.html
//
export function id3_toggle_classes(element) {
  if (element.title === "Write") {
    element.title = "Preview";
    element.classList.remove("fa-pencil");
    element.classList.add("fa-eye");
  } else {
    element.title = "Write";
    element.classList.remove("fa-eye");
    element.classList.add("fa-pencil");
  }
}

export function increment_update() {
  const id_ajax_call = document.getElementById("id_nb_ajax_call");
  if (id_ajax_call === null) {
      return;
  }

  let a = Number(id_ajax_call.innerText);
  a = a + 1;
  id_ajax_call.innerText = a;
}

export function build_url_with_parameters(i_url, parameters) {
  //
  // let parameters = {'id': codifnomenclature_id,'demande_codification': demande_codification};
  //
  // Exemple d'url: "/articles/ajax/codif_nomenclature_update?id=398&besoin_reel=51";
  //
  let base_url = new URL("http://bidon");
  Object.keys(parameters).forEach((key) =>
    base_url.searchParams.append(key, parameters[key])
  );

  let url = i_url + base_url.search;
  return url;
}

export function display_parameters(parameters) {
  //
  // let parameters = {'id': 6,'demande_codification': 78};
  //
  Object.keys(parameters).forEach((key) =>
    console.log(`key=${key} parameter=${parameters[key]}`)
  );
}


export function update_demande(url) {
    const etat_demande = document.getElementById('id_etat_demande').value;
    const id_demande_article = document.getElementById('id_demande_article').textContent;
    const widget = document.getElementById('id_send_email');
    const send_email = widget.checked;
    const parameters = { 'id_demande_article': id_demande_article,  'etat_demande': etat_demande, 'send_email': send_email};
    const url_param = build_url_with_parameters(url, parameters);
    try {
        fetch(url_param);
        increment_update();
    } catch (error) {
        console.error(error);
    }
}

export function update_iframe_description_demande(url, libelles_description)
{
    const iframe_description_demande = document.getElementById('id_description_iframe');
    // Le texte HTML issu du iframe id_description_iframe
    const editables = iframe_description_demande.contentDocument.getElementsByClassName('note-editable');
    if (editables.length > 0) {
        const editable = editables[0];
        const description = editable.innerHTML;
        const id_demande_article = document.getElementById('id_demande_article').textContent;
        console.log(`id_demande_article=${id_demande_article} description=${description}`);
        const widget = document.getElementById('id_send_email');
        const send_email = widget.checked;

        const parameters = {'id_demande_article': id_demande_article,'description': description,'send_email': send_email};
        const url_param = build_url_with_parameters(url, parameters);
        fetch(url_param).then(response => response.json()).then(json => {
            increment_update();
            libelles_description.forEach(function(libelle, index, array) {
                libelle.textContent = json.libelle_description;
            });
        });
    }
}

export function update_summernote(url, editables, map_parameters, key_data) {
    if (editables.length > 0) {
        const editable = editables[0];
        const new_data = editable.innerHTML;
        map_parameters.set(key_data, new_data);
        const parameters =  Object.fromEntries(map_parameters);
        const url_param = build_url_with_parameters(url, parameters);
        fetch(url_param).then(response => response.json()).then(json => {
            increment_update();
        });
    };
}


export function id3_fetch(url, parameters) {
    // https://gdevops.frama.io/web/javascript/reference/promise/flaviocopes.com/flaviocopes.com.html
    // chaining of promises
    const url_param = build_url_with_parameters(url, parameters);
    const promise = fetch(url_param)
    .then(response => response.json())
    .then(json => {
        increment_update();
        return Promise.resolve(json);
    });
    return promise;
}

export function get_temps_impute(temps_impute) {
    //  Par defaut un intervalle est de type HH:MM:SS => on concatene ':00'
    let nb_tokens = temps_impute.split(':').length;
    if (nb_tokens === 1) {
      temps_impute = temps_impute.concat(':00:00');
    }
    if (nb_tokens === 2) {
      temps_impute = temps_impute.concat(':00');
    }
    // Cas de l'emploi de la virgule
    nb_tokens = temps_impute.split(',').length
    if (nb_tokens === 2) {
      //  de cette façon on a le meme comportement qu'avec le "." ce qui est très bien
      temps_impute = tokens[0] + '.' + tokens[1];
    }

    console.log(`temps_impute=${temps_impute}`);
    return temps_impute;
}
