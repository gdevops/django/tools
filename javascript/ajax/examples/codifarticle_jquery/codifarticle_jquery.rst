
.. index::
   pair: Ajax ; Codifarticle

.. _codifarticle_ajax_example:

============================================
**Codifarticle AJAX** with jQuery example
============================================



articles.urls.py
==================

::

    from .views_codif_article import view_update_codifarticle


    path(
        "ajax/codif_article_update",
        view_update_codifarticle,
        name="ajax_update_codif_article",
    ),


views.view_update_codifarticle
==================================

.. code-block:: python
   :linenos:

    import logging
    from typing import Any, Dict
    from datetime import datetime
    from django.utils import timezone

    from django.contrib import messages

    from django.contrib.auth.mixins import LoginRequiredMixin
    from django.contrib.auth.decorators import login_required

    from django.http.request import HttpRequest
    from django.http import HttpResponseRedirect, JsonResponse
    from django.apps import apps
    from django.forms.models import modelform_factory
    from django.http import HttpResponseRedirect
    from django.shortcuts import get_object_or_404, redirect
    from django.urls import reverse
    from django.views.generic.base import TemplateResponseMixin, View
    from django.views.generic.edit import DeleteView, UpdateView

    from employes.models import Employe

    from .forms_codif_article import CodifArticleFormSetExtra0, CodifArticleFormSetExtra1
    from .forms_demande_article import DemandeArticleUpdateFormInLine
    from .models_codif_article import CodifArticle, DocCodifArticle, ReponseCodifArticle
    from .models_demande_article import DemandeArticle

    from projets.models import Projet
    from articles.models import Article
    from fabricants.models import Fabricant
    from fournisseurs.models import Fournisseur


    # Get an instance of a logger
    logger = logging.getLogger(__name__)


    def view_update_codifarticle(request: HttpRequest) -> JsonResponse:
        """Mise à jour des codifications article par requete AJAX.
        """
        logger.info("Enter view_update_codifarticle()")

        codif_article_id = request.GET.get("id", None)
        logger.info(f"{codif_article_id=}")

        try:
            codif_article = CodifArticle.objects.get(pk=codif_article_id)
            if codif_article.demande_cloturee():
                data = {}
                data["update"] = False
                logger.info(f"No change authorized")
                return JsonResponse(data)
            else:
                logger.info(f"Demande non cloturée {codif_article.demande_article}")

        except:
            pass


        etat_poste = request.GET.get("etat_poste", None)
        reponse = request.GET.get("reponse", None)
        ofda = request.GET.get("ofda", None)

        projet = request.GET.get("projet_id", None)
        if projet is not None:
            try:
                projet = Projet.objects.get(pk=projet)
            except Exception as error:
                logger.error(f"view_update_codifarticle {error=}")

        article = request.GET.get("article_id", None)
        if article is not None:
            try:
                article = Article.objects.get(pk=article)
            except Exception as error:
                logger.error(f"view_update_sortie_stock {error=}")

        designation = request.GET.get("designation", None)
        fabricant = request.GET.get("fabricant_id", None)
        if fabricant is not None:
            try:
                fabricant = Fabricant.objects.get(pk=fabricant)
            except Exception as error:
                logger.error(f"view_update_codifarticle {error=}")

        ref_fabricant = request.GET.get("ref_fabricant", None)
        fournisseur = request.GET.get("fournisseur_id", None)
        if fournisseur is not None:
            try:
                fournisseur = Fournisseur.objects.get(pk=fournisseur)
            except Exception as error:
                logger.error(f"view_update_codifarticle {error=}")

        ref_fournisseur = request.GET.get("ref_fournisseur", None)
        date_butoir = None
        try:
            date_butoir_str = request.GET.get("date_butoir", None)
            format_str = "%d/%m/%Y %H:%M"
            date_butoir_unaware = datetime.strptime(
                date_butoir_str, format_str
            )
            date_butoir = timezone.make_aware(
                date_butoir_unaware, timezone.get_current_timezone()
            )
            logger.info(f"{date_butoir_str=} {date_butoir=}")
        except Exception as error:
            # logger.error(f"view_update_sortie_stock {error=}")
            pass


        demande_codification = request.GET.get("demande_codification", None)
        if demande_codification is not None:
            logger.info(f"{demande_codification=}")
            if demande_codification == "true":
                demande_codification = True
            else:
                demande_codification = False

        besoin_reel = request.GET.get("besoin_reel", None)
        besoin_potentiel = request.GET.get("besoin_potentiel", None)
        sortie_stock = request.GET.get("sortie_stock", None)
        cotation_par = request.GET.get("cotation_par", None)

        data = {}
        data["update"] = False
        try:
            save = False
            msg = ""
            if etat_poste is not None:
                logger.info(f"{etat_poste=}")
                codif_article.etat_poste = etat_poste
                msg = f"new etat_poste:{etat_poste}"
                save = True

            if reponse is not None:
                logger.info(f"{reponse=}")
                codif_article.reponse = reponse
                msg = f"new reponse:{reponse}"
                save = True

            if ofda is not None:
                logger.info(f"{ofda=}")
                codif_article.ofda = ofda
                msg = f"new ofda:{ofda}"
                save = True

            if article is not None:
                codif_article.article = article
                msg = f"new article:{article}"
                save = True

            if projet is not None:
                codif_article.projet = projet
                msg = f"new projet:{projet}"
                save = True

            if designation is not None:
                codif_article.designation = designation
                msg = f"new designation:{designation}"
                save = True

            if fabricant is not None:
                codif_article.fabricant = fabricant
                msg = f"new fabricant:{fabricant}"
                save = True

            if ref_fabricant is not None:
                codif_article.ref_fabricant = ref_fabricant
                msg = f"new ref_fabricant:{ref_fabricant}"
                save = True

            if fournisseur is not None:
                codif_article.fournisseur = fournisseur
                msg = f"new fournisseur:{fournisseur}"
                save = True

            if ref_fournisseur is not None:
                codif_article.ref_fournisseur = ref_fournisseur
                msg = f"new ref_fournisseur:{ref_fournisseur}"
                save = True

            if date_butoir is not None:
                codif_article.date_butoir = date_butoir
                msg = f"new date_butoir:{date_butoir}"
                save = True

            if demande_codification is not None:
                # logger.info(f"{demande_codification=}")
                codif_article.demande_codification = demande_codification
                msg = f"new demande_codification:{demande_codification}"
                save = True

            if besoin_reel is not None:
                codif_article.besoin_reel = besoin_reel
                msg = f"new besoin_reel:{besoin_reel}"
                save = True

            if besoin_potentiel is not None:
                logger.info(f"{besoin_potentiel=}")
                codif_article.besoin_potentiel = besoin_potentiel
                msg = f"new besoin_potentiel:{besoin_potentiel}"
                save = True

            if sortie_stock is not None:
                logger.info(f"{sortie_stock=}")
                codif_article.sortie_stock = sortie_stock
                msg = f"new sortie_stock:{sortie_stock}"
                save = True

            if cotation_par is not None:
                logger.info(f"{cotation_par=}")
                codif_article.cotation_par = cotation_par
                msg = f"new cotation_par:{cotation_par}"
                save = True

            if save:
                codif_article.save()
                data["update"] = msg
                # retour data
                if article is not None:
                    data["libelle_article"] = article.get_libelle()
                if projet is not None:
                    data["libelle_projet"] = projet.get_libelle()

        except Exception as error:
            logger.error(f"view_update_codifarticle {error=}")

        return JsonResponse(data)


articles.templates.articles.demande.achat_service.articles_update.html
==========================================================================

.. literalinclude:: articles_update.html
   :linenos:
   :language: django
