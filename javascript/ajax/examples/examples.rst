
.. index::
   pair: Ajax ; Examples

.. _django_ajax_examples:

============================================
Django Javascript AJAX examples
============================================

.. toctree::
   :maxdepth: 3

   codifarticle_jquery/codifarticle_jquery
   codif_article_fetch/codifarticle_fetch
