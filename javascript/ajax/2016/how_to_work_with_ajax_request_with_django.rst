

.. _ajax_freitas_2016:

=================================================================
**How to Work With AJAX Request With Django** by Vitor Freitas
=================================================================

.. seealso::

   - https://simpleisbetterthancomplex.com/tutorial/2016/08/29/how-to-work-with-ajax-request-with-django.html
