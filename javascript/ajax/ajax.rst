
.. index::
   pair: Django ; Ajax
   pair: Javascript ; Ajax
   ! AJAX

.. _django_javascript_ajax:

============================================
Django Javascript Ajax
============================================

.. seealso::

   - :ref:`jquery_ajax`


.. figure:: django-forms-overview.png
   :align: center

.. toctree::
   :maxdepth: 3

   2020/2020
   2016/2016
   2014/2014
   examples/examples
