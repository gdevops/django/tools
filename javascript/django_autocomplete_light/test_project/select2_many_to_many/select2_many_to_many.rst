.. index::
   pair: django_autocomplete_light ; select2_many_to_many
   ! select2_many_to_many
   ! many_to_many


.. _test_dal_select2_many_to_many:

========================================================================================================
Tests django_autocomplete_light **select2_many_to_many**
========================================================================================================

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light/tree/master/test_project/select2_many_to_many



models.py
=========

.. literalinclude:: models.py
   :linenos:

forms.py
=========

.. literalinclude:: forms.py
   :linenos:

urls.py
=========

.. literalinclude:: urls.py
   :linenos:

test_functional.py
===================

.. literalinclude:: test_functional.py
   :linenos:


Examples
=========

.. _dal_m2m_897:

Example issue 87
-------------------

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light/issues/897


.. figure:: example_issue_897.png
   :align: center
