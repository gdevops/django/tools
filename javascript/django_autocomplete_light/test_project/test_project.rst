
.. _tests_django_autocomplete_light:

========================================================================================================
Tests **django_autocomplete_light**
========================================================================================================

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light/tree/master/test_project

.. toctree::
   :maxdepth: 3

   select2_many_to_many/select2_many_to_many
