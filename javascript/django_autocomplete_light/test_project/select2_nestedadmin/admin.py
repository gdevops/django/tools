import nested_admin
from django.contrib import admin

from .forms import TFormThree
from .models import TModelOne
from .models import TModelThree
from .models import TModelTwo


class TModelThreeInline(nested_admin.NestedStackedInline):
    model = TModelThree
    form = TFormThree
    extra = 1


class TModelTwoInline(nested_admin.NestedStackedInline):
    model = TModelTwo
    inlines = [TModelThreeInline]
    extra = 1


class TModelOneAdmin(nested_admin.NestedModelAdmin):
    model = TModelOne
    inlines = [TModelTwoInline]


admin.site.register(TModelOne, TModelOneAdmin)
