
.. _django_autocomplete_light_features:

===========
Features
===========

- Python 2.7, 3.4, Django 2.0+ support (Django 1.11 (LTS), is supported until django-autocomplete-light-3.2.10),
- Django (multiple) choice support,
- Django **(multiple) model** choice support,
- Django generic foreign key support (through django-querysetsequence),
- Django **generic many to many relation** support (through django-generic-m2m and django-gm2m)
- Multiple widget support: select2.js, easy to add more.
- Creating choices that don't exist in the autocomplete,
- Offering choices that depend on other fields in the form, in an elegant and innovative way,
- Dynamic widget creation (ie. inlines), supports YOUR custom scripts too,
- Provides a test API for your awesome autocompletes, to support YOUR custom use cases too,
- A documented automatically tested example for each use case in test_project.-
