
.. index::
   pair: Django ; django_autocomplete_light

.. _django_autocomplete_light:

========================================================================================================
**django_autocomplete_light** A fresh approach to autocomplete implementations, specially for Django
========================================================================================================

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light
   - https://github.com/yourlabs/django-autocomplete-light/graphs/contributors
   - https://django-autocomplete-light.readthedocs.io/en/master/
   - https://github.com/yourlabs/django-autocomplete-light/issues/1184


.. toctree::
   :maxdepth: 3

   features/features
   test_project/test_project
