
.. index::
   pair: Django ; summernote
   ! summernote

.. _django_summernote_1:

===============================================================================
Django **summernote** Simply integrate Summernote editor with Django project
===============================================================================

.. seealso::

   - https://github.com/summernote
   - https://github.com/summernote/summernote/graphs/contributors
   - https://github.com/summernote/django-summernote
   - https://github.com/summernote/summernote
