
.. index::
   pair: Django ; Javascript

.. _django_javascript:

============================================
Javascript front-end
============================================

.. toctree::
   :maxdepth: 3


   ajax/ajax
   django_autocomplete_light/django_autocomplete_light
   sockpuppet/sockpuppet
   summernote/summernote
