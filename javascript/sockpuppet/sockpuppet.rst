
.. index::
   pair: Django ; Sockpuppet
   pair: Django ; Sockpuppet


.. _django_sockpuppet:

======================================================================================================
**django-sockpuppet** Build reactive applications with the django tooling you already know and love
======================================================================================================

.. seealso::

   - https://x.com/argparse
   - https://github.com/jonathan-s/django-sockpuppet
   - https://sockpuppet.argpar.se/
   - https://mainlydata.kubadev.com/python/django/dont-forget-when-starting-with-django-sockpuppet/
   - https://docs.stimulusreflex.com/
   - https://discord.com/invite/XveN625
   - https://django-sockpuppet-expo.python3.ninja/example/
