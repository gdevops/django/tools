
.. index::
   pair: Django ; plotly

.. _django_plotly:

============================================
**plotly**
============================================

.. seealso::

   - https://github.com/ricleal/DjangoPlotLy
   - https://github.com/plotly/plotly.py
   - https://plot.ly/python/
   - https://plot.ly/python/pie-charts/
   - https://plot.ly/python/bar-charts/
   - https://plot.ly/python/pie-charts/#pie-charts-in-subplots
   - https://hakibenita.com/django-group-by-sql
   - https://plot.ly/python/getting-started/
   - https://plot.ly/python/renderers/


.. toctree::
   :maxdepth: 3

   examples/examples
