.. index::
   pair: plotly ; logs

.. _django_plotly_logs_example:

============================================
plotly logs example
============================================




logs.urls.py
==============

.. code-block:: python
   :linenos:

    """Gestion des URLs pour la gestion des logs des (hardware-id, product_name).

    - https://gdevops.frama.io/django/tuto/tutos/hacksoft/hacksoft.html#urls

    """
    from django.urls import path, re_path
    from . import views

    app_name = "logs"

    urlpatterns = [
        path("plot_program/", views.PlotProgram.as_view(), name="plot_program",),
        path(
            "plot_all_programs/", views.PlotAllPrograms.as_view(), name="plot_all_programs",
        ),
        path(
            "plot_program/<str:product_name>/",
            views.PlotProgram.as_view(),
            name="plot_program_name",
        ),
    ]


logs.plots.py
===============

.. literalinclude:: plots.py
   :linenos:
   :language: python


logs.models.py
==================

.. literalinclude:: models.py
   :linenos:
   :language: python

logs.views.py
==================


.. code-block:: python
   :linenos:

    """
    logs.views

    """

    from django.shortcuts import render
    import logging

    from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
    from django.views.generic import TemplateView
    from . import plots

    logger = logging.getLogger(__name__)


    class PlotProgram(TemplateView):
        """

        - https://plot.ly/python/pie-charts/#pie-charts-in-subplots

        """

        template_name = "logs/plot.html"

        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            product_name = None
            if "product_name" in self.kwargs:
                product_name = self.kwargs["product_name"]
                logger.info(f"PlotPrograms {product_name=}")
                plot = plots.plot_program(product_name)
            else:
                plot = plots.plot_program()

            context["plot"] = plot
            if product_name is None:
                product_name = "i version"
            context["product_name"] = product_name
            return context


    class PlotAllPrograms(TemplateView):
        """

        - https://plot.ly/python/pie-charts/#pie-charts-in-subplots

        """

        template_name = "logs/plot_all_programs.html"

        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            les_plots = plots.plot_all_programs()
            context["les_plots"] = les_plots
            return context

template
==========

.. code-block:: django
   :linenos:

    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <div class="dropdown-divider"></div>
      {% url 'logs:plot_all_programs' as url_plot_all_programs %}
      <a class="dropdown-item" href="{{ url_plot_all_programs }}">All programs</a>
      <div class="dropdown-divider"></div>
      {% url 'logs:plot_program' as url_plot_program %}
      <a class="dropdown-item" href="{{ url_plot_program }}i version">Program1 version</a>
      <a class="dropdown-item" href="{{ url_plot_program }}i version - alpha">Program1 version PIN</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="{{ url_plot_program }}i Firma">Program1 Firma</a>
      <a class="dropdown-item" href="{{ url_plot_program }}i Firma - PIN">Program1 Firma PIN</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="{{ url_plot_program }}i Usuarios">Program1 Usuarios</a>
      <a class="dropdown-item" href="{{ url_plot_program }}i Usuarios - PIN">Program1 Usuarios PIN</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="{{ url_plot_program }}i Web Services">Program1 Web Services</a>
    </div>
