"""Logs des requetes sur getLastProductVersion.

"""
import logging
from dataclasses import dataclass
from dataclasses import field
from typing import List

import arrow
import django
from dateutil import tz
from django.db import connection
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext as _
from programs.models import Program

# http://crsmithdev.com/arrow/
# https://labix.org/python-dateutil
# https://docs.djangoproject.com/en/dev/topics/db/sql/
# https://docs.djangoproject.com/en/dev/topics/class-based-views/generic-editing/

# Get an instance of a logger
logger = logging.getLogger(__name__)


@dataclass
class StatsProductNameVersion:
    product_name: str
    version: str = ""
    nb_logs: int = 0
    stats_product_name: "StatsProductName" = None

    @property
    def percent_logs(self):
        percent = ""
        p = self.nb_logs / self.stats_product_name.nb_total_logs
        percent = f"({p:.2%})"
        return percent


@dataclass
class StatsProductName:
    product_name: str
    nb_total_logs: int = 0
    versions: List[StatsProductNameVersion] = field(default_factory=list)


class LogHardwareId(models.Model):
    """Description des logs à partir du hardware-id.

        http -b "localhost:8005/programs/getLastProductVersion/?product_name=id3 Notaria Autenticacion&version__gt=2.4.0&culture=es-CO&hardware_id=35ESDNN-486MAB5-4F3FWCJ-E5N8E02" 'Authorization: Token c62a063ef406c807f89b5e22706e0ad5de443442'

    On logge les informations suivantes lorsqu'une requete arrive sur getLastProductVersion

    - hardware_id
    - product_name
    - date de la requete
    - programme

    """

    class Meta(object):
        managed = True
        db_table = "log_hardware"
        ordering = ["-date_connexion"]
        verbose_name = _("log hardware id")
        verbose_name_plural = _("Les logs des hardware id")
        # https://docs.djangoproject.com/en/dev/ref/models/constraints/#uniqueconstraint
        constraints = [
            models.UniqueConstraint(
                fields=["hardware_id", "product_name"],
                name="unique_hardware_id_productname",
            )
        ]
        indexes = [
            models.Index(fields=["date_connexion"], name="index_date_connexion"),
            models.Index(fields=["hardware_id"], name="index_hardware_id"),
        ]

    id = models.AutoField(primary_key=True)
    hardware_id = models.CharField(max_length=100, help_text=_("Identifiant du PC"))
    date_connexion = models.DateTimeField(
        help_text=_("Dernière date de connexion"), auto_now_add=True
    )
    product_name = models.CharField(
        max_length=200,
        default="",
        help_text=_("Le nom du programme"),
    )
    version = models.CharField(
        max_length=50,
        default="0.1.0",
        help_text=_("La version du programme. Ex: 0.2.0. Voir http://semver.org/"),
        verbose_name=_("Version"),
    )
    program = models.ForeignKey(
        Program,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="programs",
        help_text=_("Le programme"),
    )
    error = models.CharField(
        max_length=200,
        default="",
        blank=True,
        null=True,
        help_text=_("Erreur"),
    )

    def __str__(self):
        """
        Affichage des logs hardware-id + product_name.
        """
        message = f"{self.id=} {self.date_connexion=} {self.product_name=} {self.version=} {self.program=}"
        return message
