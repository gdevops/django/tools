"""
logs.plots.py


- https://plot.ly/python/pie-charts

Traduction de pie chart
=========================

- https://www.linguee.fr/anglais-francais/traduction/pie+chart.html

:pie chart:
    diagramme circulaire
    graphique circuliare
    graphique sectoriel
    diagramme en camenbert


"""
import logging
from typing import List

import numpy as np
import pendulum
import plotly.graph_objs as go
from logs.models import LogHardwareId
from logs.models import StatsProductName
from logs.models import StatsProductNameVersion
from plotly.offline import plot
from plotly.subplots import make_subplots
from programs.models import Program

logger = logging.getLogger(__name__)


def plot_program(product_name: str = "id3 Notaria Autenticacion") -> plot:
    """
    - https://plot.ly/python/pie-charts/#pie-charts-in-subplots
    """
    logs_hardware = LogHardwareId.objects.filter(
        product_name=product_name,
    ).order_by("product_name", "-version")

    nb_total_users = logs_hardware.count()

    # définition de l'ensemble des versions
    set_versions = set()
    for log in logs_hardware:
        set_versions.add(log.version)

    # Exemple: ["2.7.1", "2.6.1"]
    labels_versions = list(set_versions)
    nb_versions = len(labels_versions)

    # calcul du nombre de hardware-id utilisant cette version
    values_versions = []
    for version in labels_versions:
        total_version = logs_hardware.filter(version=version).count()
        values_versions.append(total_version)

    # on concatène aux labels le nombre d'occurrences et le pourcentage
    for i, total_version in enumerate(values_versions):
        p = total_version / nb_total_users
        labels_versions[
            i
        ] = f"version {labels_versions[i]} ({str(total_version)} users => {p:.1%})"

    fig = go.Figure(
        data=[go.Pie(labels=labels_versions, values=values_versions, name=product_name)]
    )
    # fig.update_traces(hole=0.4, hoverinfo="label+value+name")
    fig.update_traces(hole=0.4, hoverinfo="label")
    if nb_versions <= 1:
        str_nb_versions = f"{nb_versions} version"
    else:
        str_nb_versions = f"{nb_versions} versions"

    if nb_total_users > 0:
        title_text = f"{str_nb_versions} for {product_name} ({nb_total_users} users)"
    else:
        now = pendulum.now()
        title_text = (
            f"{str_nb_versions} for {product_name} (not used at the moment "
            f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H{now.minute:02})"
        )

    fig.update_layout(
        title_text=title_text,
    )
    plot_div = plot(fig, output_type="div", include_plotlyjs=False)
    return plot_div


def plot_all_programs() -> List[plot]:
    """Returns all the program plots"""
    plots = []
    for product_name in (
        Program.objects.filter(version_is_ready=True)
        .values_list("product_name", flat=True)
        .distinct()
    ):
        plot = plot_program(product_name)
        plots.append(plot)

    return plots
