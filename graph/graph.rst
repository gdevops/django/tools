
.. index::
   pair: Django ; Graph

.. _django_graph:

============================================
Django graph
============================================

.. toctree::
   :maxdepth: 3

   plotly/plotly
