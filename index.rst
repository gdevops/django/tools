.. index::
   pair: Django ; Tools

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/django/tools/rss.xml>`_


.. _django_tools:

============================================
**Django Tools**
============================================

- https://hakibenita.com/9-django-tips-for-working-with-databases

.. toctree::
   :maxdepth: 3

   django_packages/django_packages
   django_hijack/django_hijack
   django-drifter/django-drifter 
   djhtml/djhtml
   djlint/djlint
   graph/graph
   javascript/javascript
   sqlparse/sqlparse
